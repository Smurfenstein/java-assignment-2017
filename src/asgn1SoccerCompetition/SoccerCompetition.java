/**
 * 
 */
package asgn1SoccerCompetition;

import java.util.ArrayList;

import asgn1Exceptions.CompetitionException;
import asgn1Exceptions.LeagueException;

/**
 * A class to model a soccer competition. The competition contains one or more number of leagues, 
 * each of which contain a number of teams. Over the course a season matches are played between
 * teams in each league. At the end of the season a premier (top ranked team) and wooden spooner 
 * (bottom ranked team) are declared for each league. If there are multiple leagues then relegation 
 * and promotions occur between the leagues.
 * 
 * @author Alan Woodley
 * @author Heath Mayocchi
 * @version 1.1
 *
 */
public class SoccerCompetition implements SportsCompetition{
	
	String name;
	int numLeagues;
	int numTeams;
	private ArrayList<SoccerLeague> competition;	
	// ArrayLists to hold teams that need to be moved
	private ArrayList<SoccerTeam> topTeams;
	private ArrayList<SoccerTeam> bottomTeams;
	
	/**
	 * Creates the model for a new soccer competition with a specific name,
	 * number of leagues and number of teams in each league
	 * 
	 * @param name The name of the competition.
	 * @param numLeagues The number of leagues in the competition.
	 * @param numTeams The number of teams in each league.
	 */
	public SoccerCompetition(String name, int numLeagues, int numTeams){
		this.name = name;
		this.numLeagues = numLeagues;
		this.numTeams = numTeams;
		competition = new ArrayList<SoccerLeague>(numLeagues);
		for (int league = 0; league < numLeagues; league++){
			SoccerLeague thisLeague = new SoccerLeague(this.numTeams);
			competition.add(thisLeague);
		}
	}
	
	/**
	 * Retrieves a league with a specific number (indexed from 0). Returns an exception if the 
	 * league number is invalid.
	 * 
	 * @param leagueNum The number of the league to return.
	 * @return A league specified by leagueNum.
	 * @throws CompetitionException if the specified league number is less than 0.
	 *  or equal to or greater than the number of leagues in the competition.
	 */
	public SoccerLeague getLeague(int leagueNum) throws CompetitionException{
		if (leagueNum < 0 || leagueNum >= numLeagues){
			throw new CompetitionException();
		}
		return competition.get(leagueNum);
	}	

	/**
	 * Starts a new soccer season for each league in the competition.
	 */
	public void startSeason(){
		for (SoccerLeague league : competition){
			try {
				league.startNewSeason();
			} catch (LeagueException e) {
				e.printStackTrace();
			}
		}
	}
	
	/** 
	 * Ends the season of each of the leagues in the competition. 
	 * If there is more than one league then it handles promotion
	 * and relegation between the leagues.  
	 * 
	 */
	public void endSeason(){
		if (numLeagues > 1){
			// Sort teams and set offSeason to true
			for (SoccerLeague league : competition){
				try {
					league.sortTeams();
					league.endSeason();
				} catch (LeagueException e) {
					e.printStackTrace();
				}
			}
			// Apply promotions and relegations to teams
			createBestAndWorstTeamArray();
			promotionsAndRelegations();
		}
		else{
			try {
				competition.get(numLeagues-1).endSeason();
			} catch (LeagueException e) {
				e.printStackTrace();
			}
		}
	}

	/** 
	 * For each league displays the competition standings.
	 */
	public void displayCompetitionStandings(){
		System.out.println("+++++ " + this.name + " +++++");
		for (int printLeague = 0; printLeague < numLeagues; printLeague++){
			System.out.println("---- League " + (printLeague +1) + " ----");
			competition.get(printLeague).displayLeagueTable();			
		}
	}
	
	/**
	 * Add worst team from each league, except the last league 
	 * Add best team from each league, except the first league
	 */
	private void createBestAndWorstTeamArray(){
		topTeams = new  ArrayList<SoccerTeam>(numTeams);
		bottomTeams = new  ArrayList<SoccerTeam>(numTeams);
		// Add best and worst teams to array
		for (int leagueTeam = 0; leagueTeam < numLeagues - 1; leagueTeam++){				
			try {
				bottomTeams.add(getLeague(leagueTeam).getBottomTeam());
				topTeams.add(getLeague(leagueTeam +1).getTopTeam());
			} catch (LeagueException | CompetitionException e) {
				e.printStackTrace();
			}
		}	
	}
	
	/**
	 * Promote best teams to a higher league
	 * Relegate worst teams to lower league
	 */
	private void promotionsAndRelegations(){
		for (int leagueTeams = 0; leagueTeams < numLeagues - 1; leagueTeams++){				
			try {
				// Use the team arrays to identify which teams to remove and add into which leagues
				getLeague(leagueTeams).removeTeam(bottomTeams.get(leagueTeams));
				getLeague(leagueTeams).registerTeam(topTeams.get(leagueTeams));
				getLeague(leagueTeams +1).removeTeam(topTeams.get(leagueTeams));
				getLeague(leagueTeams +1).registerTeam(bottomTeams.get(leagueTeams));
			} catch (LeagueException | CompetitionException e) {
				e.printStackTrace();
			}
		}	
	}
}
