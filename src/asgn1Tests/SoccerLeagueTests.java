package asgn1Tests;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerLeague;
import asgn1SoccerCompetition.SoccerTeam;


/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerLeage class
 *
 * @author Alan Woodley
 * @author Heath Mayocchi
 *
 */
public class SoccerLeagueTests {
	private int fourTeams = 4;
	SoccerLeague thisLeague;
	// Variable for output stream content
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	
	/* Helper method for testing displayNewTeam()
	 * Assigns outContent to the standard output stream */
	public void setUpStream() {
	    System.setOut(new PrintStream(outContent));
	}
	/* Helper method for testing displayNewTeam()
	 * Assigns null to the standard output stream */
	public void cleanUpStream() {
	    System.setOut(null);
	}
	
	@Before
	public void newLeague(){
		thisLeague = new SoccerLeague(fourTeams);		
		setUpStream();
	}
	
	@After
	public void resetStream(){
		cleanUpStream();
	}

	// Testing registerTeam(SoccerTeam team)	
	@Test
	public void registerOneTeam() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		thisLeague.registerTeam(aTeam);
		assertEquals(1, thisLeague.getRegisteredNumTeams());
	}
	
	@Test
	public void registerTwoTeams() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		SoccerTeam secondTeam = new SoccerTeam("Second Teams Official", "Second Teams Nick");
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(secondTeam);
		assertEquals(2, thisLeague.getRegisteredNumTeams());
	}	
	
	@Test(expected=LeagueException.class)
	public void registerOnSeason() throws LeagueException, TeamException{
		thisLeague.startNewSeason();
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		thisLeague.registerTeam(aTeam);
	}
	
	@Test(expected=LeagueException.class)
	public void registerOverRequiredTeams() throws TeamException, LeagueException{
		SoccerTeam firstTeam = new SoccerTeam("First Team", "First");
		SoccerTeam secondTeam = new SoccerTeam("Second Team", "Second");
		SoccerTeam thirdTeam = new SoccerTeam("Third team", "Third");
		SoccerTeam fourthTeam = new SoccerTeam("Fourth Team", "Fourth");
		SoccerTeam fifthTeam = new SoccerTeam("Fifth Team", "Fifth");
		thisLeague.registerTeam(firstTeam);
		thisLeague.registerTeam(secondTeam);	
		thisLeague.registerTeam(thirdTeam);
		thisLeague.registerTeam(fourthTeam);
		thisLeague.registerTeam(fifthTeam);		
	}

	// Testing removeTeam(SoccerTeam team)	
	@Test
	public void removeOneTeam() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		SoccerTeam secondTeam = new SoccerTeam("Second Teams Official", "Second Teams Nick");
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(secondTeam);
		thisLeague.removeTeam(aTeam);
		assertEquals(1, thisLeague.getRegisteredNumTeams());
	}
	
	@Test
	public void removeTwoTeams() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		SoccerTeam secondTeam = new SoccerTeam("Second Teams Official", "Second Teams Nick");
		SoccerTeam thirdTeam = new SoccerTeam("Third Teams Official", "Third Teams NIck");
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(secondTeam);
		thisLeague.registerTeam(thirdTeam);
		thisLeague.removeTeam(aTeam);
		thisLeague.removeTeam(secondTeam);
		assertEquals(1, thisLeague.getRegisteredNumTeams());
	}
	
	@Test(expected=LeagueException.class)
	public void removeNewTeam() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		SoccerTeam secondTeam = new SoccerTeam("Second Teams Official", "Second Teams Nick");	
		thisLeague.registerTeam(aTeam);	
		thisLeague.removeTeam(secondTeam);
	}
	
	@Test(expected=LeagueException.class)
	public void removeOnSeason() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		thisLeague.registerTeam(aTeam);	
		thisLeague.startNewSeason();
		thisLeague.removeTeam(aTeam);
	}
	
	// Testing getRegisteredNumTeams()	
	@Test
	public void getNoRegisteredTeams() throws TeamException, LeagueException{
		assertEquals(0, thisLeague.getRegisteredNumTeams());
	}
	
	@Test
	public void getOneRegisteredTeam() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		SoccerTeam secondTeam = new SoccerTeam("Second Team", "Second");
		thisLeague.registerTeam(aTeam);		
		thisLeague.registerTeam(secondTeam);		
		assertEquals(2, thisLeague.getRegisteredNumTeams());
	}
	
	@Test
	public void getTwoRegisteredTeam() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		thisLeague.registerTeam(aTeam);		
		assertEquals(1, thisLeague.getRegisteredNumTeams());
	}
	
	// Testing getRequiredNumTeams()	
	@Test
	public void getNoRequiredTeams(){
		SoccerLeague noLeague = new SoccerLeague(0);
		assertEquals(0, noLeague.getRequiredNumTeams());
	}
	
	@Test
	public void getOneRequiredTeam(){
		SoccerLeague noLeague = new SoccerLeague(1);
		assertEquals(1, noLeague.getRequiredNumTeams());
	}
	
	@Test
	public void getTwoRequiredTeams(){
		SoccerLeague noLeague = new SoccerLeague(2);
		assertEquals(2, noLeague.getRequiredNumTeams());
	}
	
	// Testing startNewSeason()	
	@Test
	public void startNewSeason() throws LeagueException{
		thisLeague.startNewSeason();
		assertEquals(false, thisLeague.isOffSeason());
	}
	
	@Test(expected=LeagueException.class)
	public void startSeasonOnSeason() throws LeagueException{
		thisLeague.startNewSeason();
		thisLeague.startNewSeason();
	}
	
	@Test(expected=LeagueException.class)
	public void startSeasonOverLeagueSize() throws TeamException, LeagueException{
		SoccerTeam firstTeam = new SoccerTeam("First Team", "First");
		SoccerTeam secondTeam = new SoccerTeam("Second Team", "Second");
		SoccerTeam thirdTeam = new SoccerTeam("Third team", "Third");
		SoccerTeam fourthTeam = new SoccerTeam("Fourth Team", "Fourth");
		SoccerTeam fifthTeam = new SoccerTeam("Fifth Team", "Fifth");
		thisLeague.registerTeam(firstTeam);
		thisLeague.registerTeam(secondTeam);	
		thisLeague.registerTeam(thirdTeam);
		thisLeague.registerTeam(fourthTeam);
		thisLeague.registerTeam(fifthTeam);	
		thisLeague.startNewSeason();
	}
	
	// Testing endSeason()	
	@Test
	public void endSeasonOnSeason() throws LeagueException{
		thisLeague.startNewSeason();
		thisLeague.endSeason();
		assertEquals(true, thisLeague.isOffSeason());
	}
	
	@Test(expected=LeagueException.class)
	public void endSeasonOffSeason() throws LeagueException{
		thisLeague.endSeason();
	}
	
	// Testing getTeamByOfficalName(String name)	
	@Test
	public void getFirstTeamByOfficalName() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		SoccerTeam secondTeam = new SoccerTeam("Second Teams Official", "Second Teams Nick");	
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(secondTeam);
		assertEquals(aTeam, thisLeague.getTeamByOfficalName("First Teams Official"));
	}
	
	@Test
	public void getSecondTeamByOfficalName() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		SoccerTeam secondTeam = new SoccerTeam("Second Teams Official", "Second Teams Nick");	
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(secondTeam);
		assertEquals(secondTeam, thisLeague.getTeamByOfficalName("Second Teams Official"));
	}
	
	@Test(expected=LeagueException.class)
	public void getUnregisteredTeamByOfficalName() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		thisLeague.registerTeam(aTeam);
		thisLeague.getTeamByOfficalName("Second Teams Official");
	}
	
	// Testing playMatch(String homeTeamName, int homeTeamGoals, String awayTeamName, int awayTeamGoals)	
	@Test
	public void playMatch() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		SoccerTeam secondTeam = new SoccerTeam("Second Teams Official", "Second Teams Nick");	
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(secondTeam);
		thisLeague.startNewSeason();
		thisLeague.playMatch("First Teams Official", 1, "Second Teams Official", 0);
		thisLeague.getTeamByOfficalName("First Teams Official").displayTeamDetails();
		assertEquals("First Teams Official	First Teams Nick	W----	1	1	0	0	1	0	1	3\r\n", outContent.toString());
	}
	
	@Test(expected=LeagueException.class)
	public void playMatchOffSeason() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		SoccerTeam secondTeam = new SoccerTeam("Second Teams Official", "Second Teams Nick");	
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(secondTeam);	
		thisLeague.playMatch("First Teams Official", 1, "Second Teams Official", 0);	
	}
	
	@Test(expected=LeagueException.class)
	public void playMatchSameName() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("First Teams Official", "First Teams Nick");
		SoccerTeam secondTeam = new SoccerTeam("First Teams Official", "First Teams Nick");	
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(secondTeam);	
		thisLeague.playMatch("First Teams Official", 1, "First Teams Official", 0);	
	}
	
	// Testing getTopTeam()
	@Test
	public void getTopTeamByName() throws TeamException, LeagueException{	
		SoccerTeam aTeam = new SoccerTeam("The A Team", "A Team");
		SoccerTeam bTeam = new SoccerTeam("The B Team", "B Team");
		SoccerTeam cTeam = new SoccerTeam("The C Team", "C Team");
		SoccerTeam dTeam = new SoccerTeam("The D Team", "D Team");
		thisLeague.registerTeam(bTeam);
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(dTeam);
		thisLeague.registerTeam(cTeam);
		thisLeague.sortTeams();
		assertEquals(aTeam, thisLeague.getTopTeam());		
	}
	
	@Test
	public void getTopTeamByScore() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("Gotham City", "Dark Knights");
		SoccerTeam bTeam = new SoccerTeam("Metropolis", "Men of Steel");
		SoccerTeam cTeam = new SoccerTeam("Paradise Island", "Wicked Wonders");
		SoccerTeam dTeam = new SoccerTeam("Central City", "Speedsters");
		thisLeague.registerTeam(bTeam);
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(dTeam);
		thisLeague.registerTeam(cTeam);	
		thisLeague.startNewSeason();
		thisLeague.playMatch("Gotham City", 1, "Metropolis", 0);	
		thisLeague.playMatch("Paradise Island", 4, "Central City" , 3);
		thisLeague.playMatch("Gotham City",1,"Paradise Island",4);	
		thisLeague.playMatch("Metropolis",2,"Central City",2);
		thisLeague.playMatch("Metropolis",2,"Paradise Island",2);	
		thisLeague.playMatch("Gotham City",0,"Central City",2);
		thisLeague.playMatch("Metropolis",2,"Gotham City",2);	
		thisLeague.playMatch("Central City",3,"Paradise Island",4);
		thisLeague.playMatch("Paradise Island",1,"Gotham City",4);	
		thisLeague.playMatch("Central City",3,"Metropolis",1);
		thisLeague.playMatch("Paradise Island",3,"Metropolis",0);	
		thisLeague.playMatch("Central City",2,"Gotham City",2);
		thisLeague.sortTeams();	
		assertEquals(cTeam, thisLeague.getTopTeam());
	}
	
	// Testing getBottomTeam()
	@Test
	public void getBottomTeamByName() throws TeamException, LeagueException{	
		SoccerTeam aTeam = new SoccerTeam("The A Team", "A Team");
		SoccerTeam bTeam = new SoccerTeam("The B Team", "B Team");
		SoccerTeam cTeam = new SoccerTeam("The C Team", "C Team");
		SoccerTeam dTeam = new SoccerTeam("The D Team", "D Team");
		thisLeague.registerTeam(bTeam);
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(dTeam);
		thisLeague.registerTeam(cTeam);
		thisLeague.sortTeams();
		assertEquals(dTeam, thisLeague.getBottomTeam());		
	}
	
	@Test
	public void getBottomTeamByScore() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("Gotham City", "Dark Knights");
		SoccerTeam bTeam = new SoccerTeam("Metropolis", "Men of Steel");
		SoccerTeam cTeam = new SoccerTeam("Paradise Island", "Wicked Wonders");
		SoccerTeam dTeam = new SoccerTeam("Central City", "Speedsters");
		thisLeague.registerTeam(bTeam);
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(dTeam);
		thisLeague.registerTeam(cTeam);	
		thisLeague.startNewSeason();
		thisLeague.playMatch("Gotham City", 1, "Metropolis", 0);	
		thisLeague.playMatch("Paradise Island", 4, "Central City" , 3);
		thisLeague.playMatch("Gotham City",1,"Paradise Island",4);	
		thisLeague.playMatch("Metropolis",2,"Central City",2);
		thisLeague.playMatch("Metropolis",2,"Paradise Island",2);	
		thisLeague.playMatch("Gotham City",0,"Central City",2);
		thisLeague.playMatch("Metropolis",2,"Gotham City",2);	
		thisLeague.playMatch("Central City",3,"Paradise Island",4);
		thisLeague.playMatch("Paradise Island",1,"Gotham City",4);	
		thisLeague.playMatch("Central City",3,"Metropolis",1);
		thisLeague.playMatch("Paradise Island",3,"Metropolis",0);	
		thisLeague.playMatch("Central City",2,"Gotham City",2);
		thisLeague.sortTeams();	
		assertEquals(bTeam, thisLeague.getBottomTeam());
	}
	
	
	// Testing sortTeams()
	// oName, nName, form, (W+L+D), W, L, D, goals, conceded, goalDiff, compPoints	
	@Test
	public void sortNoPoints() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("The A Team", "A Team");
		SoccerTeam bTeam = new SoccerTeam("The B Team", "B Team");
		SoccerTeam cTeam = new SoccerTeam("The C Team", "C Team");
		SoccerTeam dTeam = new SoccerTeam("The D Team", "D Team");
		thisLeague.registerTeam(bTeam);
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(dTeam);
		thisLeague.registerTeam(cTeam);
		thisLeague.sortTeams();
		thisLeague.displayLeagueTable();
		String expected = "The A Team	A Team	-----	0	0	0	0	0	0	0	0\r\n"
		+ "The B Team	B Team	-----	0	0	0	0	0	0	0	0\r\n"
		+ "The C Team	C Team	-----	0	0	0	0	0	0	0	0\r\n"
		+ "The D Team	D Team	-----	0	0	0	0	0	0	0	0\r\n";
		assertEquals(expected, outContent.toString());
	}
	
	@Test
	public void sortGoalDifferenceAndCompPoints() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("Gotham City", "Dark Knights");
		SoccerTeam bTeam = new SoccerTeam("Metropolis", "Men of Steel");
		SoccerTeam cTeam = new SoccerTeam("Paradise Island", "Wicked Wonders");
		SoccerTeam dTeam = new SoccerTeam("Central City", "Speedsters");
		thisLeague.registerTeam(bTeam);
		thisLeague.registerTeam(aTeam);
		thisLeague.registerTeam(dTeam);
		thisLeague.registerTeam(cTeam);	
		thisLeague.startNewSeason();
		thisLeague.playMatch("Gotham City", 1, "Metropolis", 0);	
		thisLeague.playMatch("Paradise Island", 4, "Central City" , 3);
		thisLeague.playMatch("Gotham City",1,"Paradise Island",4);	
		thisLeague.playMatch("Metropolis",2,"Central City",2);
		thisLeague.playMatch("Metropolis",2,"Paradise Island",2);	
		thisLeague.playMatch("Gotham City",0,"Central City",2);
		thisLeague.playMatch("Metropolis",2,"Gotham City",2);	
		thisLeague.playMatch("Central City",3,"Paradise Island",4);
		thisLeague.playMatch("Paradise Island",1,"Gotham City",4);	
		thisLeague.playMatch("Central City",3,"Metropolis",1);
		thisLeague.playMatch("Paradise Island",3,"Metropolis",0);	
		thisLeague.playMatch("Central City",2,"Gotham City",2);
		thisLeague.sortTeams();
		thisLeague.displayLeagueTable();
		String expected = "Paradise Island	Wicked Wonders	WLWDW	6	4	1	1	18	13	5	13\r\n"
		+ "Central City	Speedsters	DWLWD	6	2	2	2	15	13	2	8\r\n"
		+ "Gotham City	Dark Knights	DWDLL	6	2	2	2	10	11	-1	8\r\n"
		+ "Metropolis	Men of Steel	LLDDD	6	0	3	3	7	13	-6	3\r\n";
		assertEquals(expected, outContent.toString());
	}
	
	// Testing containsTeam(String name)	
	@Test
	public void containsTheTeam() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("The A Team", "A Team");
		thisLeague.registerTeam(aTeam);
		assertEquals(true, thisLeague.containsTeam("The A Team"));
	}
	
	@Test
	public void notContainsTheTeam() throws TeamException, LeagueException{
		SoccerTeam aTeam = new SoccerTeam("The A Team", "A Team");
		thisLeague.registerTeam(aTeam);
		assertEquals(false, thisLeague.containsTeam("The B Team"));
	}
}

