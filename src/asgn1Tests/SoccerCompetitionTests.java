package asgn1Tests;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

import asgn1Exceptions.CompetitionException;
import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerCompetition;
import asgn1SoccerCompetition.SoccerLeague;
import asgn1SoccerCompetition.SoccerTeam;

/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerCompetition class
 *
 * @author Alan Woodley
 * @author Heath Mayocchi
 *
 */
public class SoccerCompetitionTests {
	
	private SoccerCompetition thisComp;
	String compName = "Comp 1";
	int numLeagues = 1;
	int numTeams = 4;
	
	// Soccer teams for testing endSeason()
	SoccerTeam firstTeamL1, secondTeamL1, thirdTeamL1, fourthTeamL1;
	SoccerTeam firstTeamL2, secondTeamL2, thirdTeamL2, fourthTeamL2;
	SoccerTeam firstTeamL3, secondTeamL3, thirdTeamL3, fourthTeamL3;
	SoccerTeam firstTeamL4, secondTeamL4, thirdTeamL4, fourthTeamL4;
	
	// Variable for output stream content
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	
	/* Helper method for testing displayNewTeam()
	 * Assigns outContent to the standard output stream */
	public void setUpStream() {
	    System.setOut(new PrintStream(outContent));
	}
	
	/* Helper method for testing displayNewTeam()
	 * Assigns null to the standard output stream */
	public void cleanUpStream() {
	    System.setOut(null);
	}
	
	@Before
	public void setupCompetition() throws TeamException{
		thisComp = new SoccerCompetition(compName, numLeagues, numTeams);	
		setUpStream();
	}
	
	
	// Testing getLeague()	
	@Test
	public void getOneLeague() throws CompetitionException{
		assertNotNull(thisComp.getLeague(numLeagues-1));
	}
	
	@Test
	public void getTwoLeagues() throws CompetitionException{
		numLeagues = 2;
		SoccerCompetition comp = new SoccerCompetition(compName, numLeagues, numTeams);
		assertNotNull(comp.getLeague(numLeagues-1));
	}
	
	@Test(expected=CompetitionException.class)
	public void getNoLeagues() throws CompetitionException{
		numLeagues = 0;
		SoccerCompetition comp = new SoccerCompetition(compName, numLeagues, numTeams);
		assertNotNull(comp.getLeague(numLeagues));
	}
	
	// Testing startSeason()	
	@Test
	public void startASeason() throws CompetitionException{
		thisComp.startSeason();
		assertFalse(thisComp.getLeague(numLeagues-1).isOffSeason());
	}
	
	@Test
	public void restartASeason() throws CompetitionException{
		thisComp.startSeason();
		thisComp.endSeason();
		thisComp.startSeason();
		assertFalse(thisComp.getLeague(numLeagues-1).isOffSeason());
	}
	
	// Testing endSeason()
	@Test
	public void endASeason() throws CompetitionException{
		thisComp.startSeason();
		thisComp.endSeason();
		assertTrue(thisComp.getLeague(numLeagues-1).isOffSeason());
	}
	
	@Test
	public void promotionsWithTwoLeagues() throws TeamException, LeagueException, CompetitionException{
		numLeagues = 2;
		thisComp = new SoccerCompetition(compName, numLeagues, numTeams);
		createLeagueOneTeams();
		createLeagueTwoTeams();
		thisComp.startSeason();
		playLeagueOneMatch();
		playLeagueTwoMatch();
		thisComp.endSeason();	
		thisComp.startSeason();
		thisComp.displayCompetitionStandings();
		String expected = "+++++ Comp 1 +++++\r\n"
		+ "---- League 1 ----\r\n"
		+ "First Team L1	First	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Fourth Team L1	Fourth	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Second Team L1	Second	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Second Team L2	Second	-----	0	0	0	0	0	0	0	0\r\n"
		+ "---- League 2 ----\r\n"
		+ "First Team L2	First	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Fourth Team L2	Fourth	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Third Team L1	Third	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Third Team L2	Third	-----	0	0	0	0	0	0	0	0\r\n";
		assertEquals(expected, outContent.toString());
	}
	
	@Test
	public void promotionsWithThreeLeagues() throws TeamException, LeagueException, CompetitionException{
		numLeagues = 3;
		thisComp = new SoccerCompetition(compName, numLeagues, numTeams);
		createLeagueOneTeams();
		createLeagueTwoTeams();
		createLeagueThreeTeams();
		thisComp.startSeason();
		playLeagueOneMatch();
		playLeagueTwoMatch();
		playLeagueThreeMatch();
		thisComp.endSeason();	
		thisComp.startSeason();
		thisComp.displayCompetitionStandings();
		String expected = "+++++ Comp 1 +++++\r\n"
		+ "---- League 1 ----\r\n"
		+ "First Team L1	First	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Fourth Team L1	Fourth	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Second Team L1	Second	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Second Team L2	Second	-----	0	0	0	0	0	0	0	0\r\n"
		+ "---- League 2 ----\r\n"
		+ "First Team L2	First	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Second Team L3	Second	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Third Team L1	Third	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Third Team L2	Third	-----	0	0	0	0	0	0	0	0\r\n"	
		+ "---- League 3 ----\r\n"	
		+ "First Team L3	First	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Fourth Team L2	Fourth	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Fourth Team L3	Fourth	-----	0	0	0	0	0	0	0	0\r\n"
		+ "Third Team L3	Third	-----	0	0	0	0	0	0	0	0\r\n";
		assertEquals(expected, outContent.toString());			
	}
	
	@Test
	public void promotionsWithNoTeams(){
		numTeams = 0;
		thisComp = new SoccerCompetition(compName, numLeagues, numTeams);	
		thisComp.startSeason();
		thisComp.endSeason();	
		thisComp.startSeason();
		thisComp.displayCompetitionStandings();
		String expected = "+++++ Comp 1 +++++\r\n"
		+ "---- League 1 ----\r\n";
		assertEquals(expected, outContent.toString());
	}
	
	// Helper method for endSeason tests
	private void createLeagueOneTeams() throws TeamException, LeagueException, CompetitionException{
		firstTeamL1 = new SoccerTeam("First Team L1", "First");// Gotham City
		secondTeamL1 = new SoccerTeam("Second Team L1", "Second");// Paradise Island
		thirdTeamL1 = new SoccerTeam("Third Team L1", "Third");// Metropolis
		fourthTeamL1 = new SoccerTeam("Fourth Team L1", "Fourth");// Central City	
		thisComp.getLeague(0).registerTeam(firstTeamL1);
		thisComp.getLeague(0).registerTeam(secondTeamL1);	
		thisComp.getLeague(0).registerTeam(thirdTeamL1);
		thisComp.getLeague(0).registerTeam(fourthTeamL1);	
	}

	// Helper method for endSeason tests
	private void createLeagueTwoTeams() throws TeamException, LeagueException, CompetitionException{
		firstTeamL2 = new SoccerTeam("First Team L2", "First");// Queens Bld
		secondTeamL2 = new SoccerTeam("Second Team L2", "Second");// Asgard City
		thirdTeamL2 = new SoccerTeam("Third Team L2", "Third");// Canadian
		fourthTeamL2 = new SoccerTeam("Fourth Team L2", "Fourth");// Brooklyn
		thisComp.getLeague(1).registerTeam(firstTeamL2);
		thisComp.getLeague(1).registerTeam(secondTeamL2);	
		thisComp.getLeague(1).registerTeam(thirdTeamL2);
		thisComp.getLeague(1).registerTeam(fourthTeamL2);			
	}

	// Helper method for endSeason tests
	private void createLeagueThreeTeams() throws TeamException, LeagueException, CompetitionException{
		firstTeamL3 = new SoccerTeam("First Team L3", "First");// Sunnyvale
		secondTeamL3 = new SoccerTeam("Second Team L3", "Second");// Serenity
		thirdTeamL3 = new SoccerTeam("Third Team L3", "Third");// Los Santos
		fourthTeamL3 = new SoccerTeam("Fourth Team L3", "Fourth");// Rossum Corp
		thisComp.getLeague(2).registerTeam(firstTeamL3);
		thisComp.getLeague(2).registerTeam(secondTeamL3);	
		thisComp.getLeague(2).registerTeam(thirdTeamL3);
		thisComp.getLeague(2).registerTeam(fourthTeamL3);			
	}

	// Helper method for endSeason tests
	private void playLeagueOneMatch() throws LeagueException, CompetitionException{
		thisComp.getLeague(0).playMatch("First Team L1", 1, "Third Team L1", 0);	
		thisComp.getLeague(0).playMatch("Second Team L1", 4, "Fourth Team L1" , 3);
		thisComp.getLeague(0).playMatch("First Team L1",1,"Second Team L1",4);	
		thisComp.getLeague(0).playMatch("Third Team L1",2,"Fourth Team L1",2);
		thisComp.getLeague(0).playMatch("Third Team L1",2,"Second Team L1",2);	
		thisComp.getLeague(0).playMatch("First Team L1",0,"Fourth Team L1",2);
		thisComp.getLeague(0).playMatch("Third Team L1",2,"First Team L1",2);	
		thisComp.getLeague(0).playMatch("Fourth Team L1",3,"Second Team L1",4);
		thisComp.getLeague(0).playMatch("Second Team L1",1,"First Team L1",4);	
		thisComp.getLeague(0).playMatch("Fourth Team L1",3,"Third Team L1",1);
		thisComp.getLeague(0).playMatch("Second Team L1",3,"Third Team L1",0);	
		thisComp.getLeague(0).playMatch("Fourth Team L1",2,"First Team L1",2);		
	}

	// Helper method for endSeason tests
	private void playLeagueTwoMatch() throws LeagueException, CompetitionException{
		thisComp.getLeague(1).playMatch("First Team L2", 1, "Third Team L2", 0);	
		thisComp.getLeague(1).playMatch("Second Team L2", 3, "Fourth Team L2", 2);
		thisComp.getLeague(1).playMatch("First Team L2", 1,"Second Team L2", 4);	
		thisComp.getLeague(1).playMatch("Third Team L2", 1,"Fourth Team L2", 1);
		thisComp.getLeague(1).playMatch("Third Team L2", 1,"Second Team L2", 0);	
		thisComp.getLeague(1).playMatch("First Team L2" , 0,"Fourth Team L2", 1);
		thisComp.getLeague(1).playMatch("Third Team L2", 0,"First Team L2", 0);	
		thisComp.getLeague(1).playMatch("Fourth Team L2", 1,"Second Team L2", 3);
		thisComp.getLeague(1).playMatch("Second Team L2", 2,"First Team L2", 3);	
		thisComp.getLeague(1).playMatch("Fourth Team L2", 1,"Third Team L2" ,1);
		thisComp.getLeague(1).playMatch("Second Team L2", 2,"Third Team L2" ,0);	
		thisComp.getLeague(1).playMatch("Fourth Team L2", 2,"First Team L2", 3);		
	}
	
	// Helper method for endSeason tests
	private void playLeagueThreeMatch() throws LeagueException, CompetitionException{
		thisComp.getLeague(2).playMatch("First Team L3", 1, "Third Team L3", 0);	
		thisComp.getLeague(2).playMatch("Second Team L3", 3, "Fourth Team L3", 2);
		thisComp.getLeague(2).playMatch("First Team L3", 1,"Second Team L3", 2);	
		thisComp.getLeague(2).playMatch("Third Team L3", 2,"Fourth Team L3", 1);
		thisComp.getLeague(2).playMatch("Third Team L3", 1,"Second Team L3", 2);	
		thisComp.getLeague(2).playMatch("First Team L3", 0,"Fourth Team L3", 1);
		thisComp.getLeague(2).playMatch("Third Team L3", 1,"First Team L3", 0);	
		thisComp.getLeague(2).playMatch("Fourth Team L3", 0,"Second Team L3", 2);
		thisComp.getLeague(2).playMatch("Second Team L3", 3,"First Team L3", 2);	
		thisComp.getLeague(2).playMatch("Fourth Team L3", 1,"Third Team L3", 1);
		thisComp.getLeague(2).playMatch("Second Team L3", 2,"Third Team L3", 0);	
		thisComp.getLeague(2).playMatch("Fourth Team L3", 0,"First Team L3", 1);		
	}
}

