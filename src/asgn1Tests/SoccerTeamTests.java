package asgn1Tests;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerTeam;



/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerLeage class
 *
 * @author Alan Woodley
 * @author Heath Mayocchi
 *
 */
public class SoccerTeamTests {
	
	private SoccerTeam firstTeam;
	private SoccerTeam secondTeam;	
	private SoccerTeam firstTeamDoppelganger;
	// Variable for output stream content
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	
	/* Helper method for testing displayNewTeam()
	 * Assigns outContent to the standard output stream */
	public void setUpStream() {
	    System.setOut(new PrintStream(outContent));
	}
	/* Helper method for testing displayNewTeam()
	 * Assigns null to the standard output stream */
	public void cleanUpStream() {
	    System.setOut(null);
	}
	
	@Before
	public void setupTeams() throws TeamException{
		String firstOfficialName = "First Official Name";
		String firstNickName = "First Nick Name";
		String secondOfficialName = "Second Official Name";
		String secondNickName = "Second Nick Name";
		firstTeam = new SoccerTeam(firstOfficialName, firstNickName);
		firstTeamDoppelganger = new SoccerTeam(firstOfficialName, firstNickName);
		secondTeam = new SoccerTeam(secondOfficialName, secondNickName);		
		setUpStream();
	}
	
	@After
	public void resetStream(){
		cleanUpStream();
	}
	
	// Testing displayNewTeam(): 
	// oName, nName, form, (W+L+D), W, L, D, goals, conceded, goalDiff, compPoints
	
	@Test
	public void displayNewTeam(){
		firstTeam.displayTeamDetails();
		assertEquals("First Official Name	First Nick Name	-----	0	0	0	0	0	0	0	0\r\n", outContent.toString());
	}
	
	// Testing getOfficialName()	
	
	@Test
	public void officialName(){
		assertEquals("First Official Name", firstTeam.getOfficialName());
	}
	
	// testing getNickName()
	
	@Test
	public void nickName(){
		assertEquals("First Nick Name", firstTeam.getNickName());
	}
	
	// Testing getGoalsScoredSeason()
	
	@Test
	public void newSeasonGoals(){
		assertEquals(0, firstTeam.getGoalsScoredSeason());
	}

	@Test
	public void playedSeasonGoals() throws TeamException{
		firstTeam.playMatch(10, 0);
		assertEquals(10, firstTeam.getGoalsScoredSeason());
	}
	
	@Test
	public void playedSeasonMultiGoals() throws TeamException{
		firstTeam.playMatch(10, 0);
		firstTeam.playMatch(5, 5);
		firstTeam.playMatch(0, 1);
		assertEquals(15, firstTeam.getGoalsScoredSeason());
	}
	
	// Testing getGoalsConcededSeason()
	
	@Test
	public void newSeasonConcededGoals(){
		assertEquals(0, firstTeam.getGoalsConcededSeason());
	}

	@Test
	public void playedSeasonConcededGoals() throws TeamException{
		firstTeam.playMatch(0, 10);
		assertEquals(10, firstTeam.getGoalsConcededSeason());
	}
	
	@Test
	public void playedSeasonMultiConcededGoals() throws TeamException{
		firstTeam.playMatch(0, 10);
		firstTeam.playMatch(0, 10);
		assertEquals(20, firstTeam.getGoalsConcededSeason());
	}
	
	// Testing getMatchesWon()
	
	@Test
	public void newSeasonMatchesWon(){
		assertEquals(0, firstTeam.getMatchesWon());
	}

	@Test
	public void playedSeasonMatchesWon() throws TeamException{
		firstTeam.playMatch(10, 0);
		assertEquals(1, firstTeam.getMatchesWon());
	}
	
	@Test
	public void playedSeasonMultiMatchesWon() throws TeamException{
		firstTeam.playMatch(10, 0);
		firstTeam.playMatch(10, 0);
		assertEquals(2, firstTeam.getMatchesWon());
	}
	
	// Testing getMatchesLost()
	
	@Test
	public void newSeasonMatchesLost(){
		assertEquals(0, firstTeam.getMatchesLost());
	}

	@Test
	public void playedSeasonMatchesLost() throws TeamException{
		firstTeam.playMatch(0, 10);
		assertEquals(1, firstTeam.getMatchesLost());
	}
	@Test
	public void playedSeasonMultiMatchesLost() throws TeamException{
		firstTeam.playMatch(0, 10);
		firstTeam.playMatch(0, 10);
		assertEquals(2, firstTeam.getMatchesLost());
	}
	
	// Testing getCompetitionPoints()
	
	@Test
	public void newSeasonCompPoints(){
		assertEquals(0, firstTeam.getCompetitionPoints());
	}

	@Test
	public void playedSeasonCompPoints() throws TeamException{
		firstTeam.playMatch(0, 1);
		firstTeam.playMatch(0, 1);
		firstTeam.playMatch(1, 0);
		firstTeam.playMatch(10, 10);
		assertEquals(4, firstTeam.getCompetitionPoints());
	}

	// Testing getGoalDifference() 
	
	@Test
	public void newSeasonGoalDifference(){
		assertEquals(0, firstTeam.getGoalDifference());
	}

	@Test
	public void playedSeasonGoalDifference() throws TeamException{
		firstTeam.playMatch(0, 1);
		firstTeam.playMatch(0, 1);
		firstTeam.playMatch(1, 0);
		firstTeam.playMatch(10, 10);
		assertEquals(-1, firstTeam.getGoalDifference());
	}
	
	// Testing getFormString() 
	
	@Test
	public void newSeasonFormString(){
		assertEquals("-----", firstTeam.getFormString());
	}

	@Test
	public void playedSeasonFormString() throws TeamException{
		firstTeam.playMatch(0, 1);
		firstTeam.playMatch(0, 1);
		firstTeam.playMatch(1, 0);
		firstTeam.playMatch(10, 10);
		assertEquals("DWLL-", firstTeam.getFormString());
	}
	
	// Testing playMatch(int goalsFor, int goalsAgainst) throws TeamException: 
	// oName, nName, form, (W+L+D), W, L, D, goals, conceded, goalDiff, compPoints
	
	@Test
	public void playNoGoals() throws TeamException{
		firstTeam.playMatch(0, 0);
		firstTeam.displayTeamDetails();
		assertEquals("First Official Name	First Nick Name	D----	1	0	0	1	0	0	0	1\r\n", outContent.toString());
	}
	
	@Test
	public void playGoalFor() throws TeamException{
		firstTeam.playMatch(1, 0);
		firstTeam.displayTeamDetails();
		assertEquals("First Official Name	First Nick Name	W----	1	1	0	0	1	0	1	3\r\n", outContent.toString());
	}
	
	@Test
	public void playGoalAgainst() throws TeamException{
		firstTeam.playMatch(0, 1);
		firstTeam.displayTeamDetails();
		assertEquals("First Official Name	First Nick Name	L----	1	0	1	0	0	1	-1	0\r\n", outContent.toString());
	}
	
	@Test
	public void playMultipleMatches() throws TeamException{
		firstTeam.playMatch(0, 1);
		firstTeam.playMatch(0, 1);
		firstTeam.playMatch(1, 0);
		firstTeam.playMatch(10, 10);
		firstTeam.displayTeamDetails();
		assertEquals("First Official Name	First Nick Name	DWLL-	4	1	2	1	11	12	-1	4\r\n", outContent.toString());
	}
	
	@Test(expected=TeamException.class)
	public void playGoalNegativeFor() throws TeamException{
		firstTeam.playMatch(-1, 0);
	}
	
	@Test(expected=TeamException.class)
	public void playGoalNegativeAgainst() throws TeamException{
		firstTeam.playMatch(0, -1);
	}
	
	@Test(expected=TeamException.class)
	public void playGoalsOver20For() throws TeamException{
		firstTeam.playMatch(21, 0);
	}
	
	@Test(expected=TeamException.class)
	public void playGoalOver20Against() throws TeamException{
		firstTeam.playMatch(0, 21);
	}
	
	// Testing compareTo(SoccerTeam other)
	
	/* Compares one team to another team. 
	A positive number is returned if the other team has more points than this team. 
	A negative number is returned if this team has more points than the other team. */
	@Test
	public void compareScoresNegative() throws TeamException{
		boolean posative;
		firstTeam.playMatch(10, 0);
		secondTeam.playMatch(0, 10);
		posative = (firstTeam.compareTo(secondTeam) > 0) ? true : false;
		assert(!posative);
	}
	
	@Test
	public void compareScoresPosative() throws TeamException{
		boolean posative;
		firstTeam.playMatch(0, 10);
		secondTeam.playMatch(10, 0);
		posative = (firstTeam.compareTo(secondTeam) > 0) ? true : false;
		assert(posative);
	}
	
	/* If both teams have same number of points then: 
	A positive number is returned if the other team has a greater goal difference than this team. 
	A negative number is returned if this team has a greater goal difference than the other team. */
	@Test
	public void compareSamePointsNegative() throws TeamException{
		boolean posative;
		firstTeam.playMatch(10, 0);
		secondTeam.playMatch(5, 0);
		posative = (firstTeam.compareTo(secondTeam) > 0) ? true : false;
		assert(!posative);
	}
	
	@Test
	public void compareSamePointsPosative() throws TeamException{
		boolean posative;
		firstTeam.playMatch(5, 0);
		secondTeam.playMatch(10, 0);
		posative = (firstTeam.compareTo(secondTeam) > 0) ? true : false;
		assert(posative);
	}
	
	/* If both teams have the same number of points and the same goal difference then: 
	A positive number is returned if the other team's official name occurs before this team alphabetically. 
	A negative number is returned if this team official name occurs before the other team alphabetically. */
	@Test
	public void compareNewTeamsNegative(){
		boolean posative;
		posative = (firstTeam.compareTo(secondTeam) > 0) ? true : false;
		assert(!posative);
	}	

	@Test
	public void compareNewTeamsPosative(){
		boolean posative;
		posative = (secondTeam.compareTo(firstTeam) > 0) ? true : false;
		assert(posative);
	}
	
	/* If teams have the same number of points, same goal difference, and same name then 0 is returned */
	@Test
	public void compareAllTheSame(){
		assertEquals(0, firstTeam.compareTo(firstTeamDoppelganger));
	}
	
	// Testing resetStats()
	
	@Test
	public void resetNewTeam(){
		firstTeam.resetStats();
		firstTeam.displayTeamDetails();
		assertEquals("First Official Name	First Nick Name	-----	0	0	0	0	0	0	0	0\r\n", outContent.toString());		
	}
	
	@Test
	public void resetPlayingTeam() throws TeamException{
		firstTeam.playMatch(0, 1);
		firstTeam.playMatch(0, 1);
		firstTeam.playMatch(1, 0);
		firstTeam.playMatch(10, 10);
		firstTeam.resetStats();
		firstTeam.displayTeamDetails();
		assertEquals("First Official Name	First Nick Name	-----	0	0	0	0	0	0	0	0\r\n", outContent.toString());		
	}	
}
