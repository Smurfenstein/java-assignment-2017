package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn1SoccerCompetition.SportsTeamForm;
import asgn1SportsUtils.WLD;

/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerTeamForm class
 *
 * @author Alan Woodley
 * @author Heath Mayocchi
 *
 */
public class SportsTeamFormTests {
	
	private SportsTeamForm result;
	
	// Create a SportsTeamForm
	@Before
	public void createSportsTeamForm(){
		result = new SportsTeamForm();
	}	
	
	// Test addResultToForm(WLD result)
	// Inherently tested from toString, getNumGames and resetForm
	
	// Test toString()

	@Test
	public void printNoResults(){
		assertEquals("-----", result.toString());
	}

	@Test
	public void printOneResults(){
		result.addResultToForm(WLD.WIN);
		assertEquals("W----", result.toString());
	}	

	@Test
	public void printFiveResults(){
		result.addResultToForm(WLD.DRAW);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.LOSS);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.WIN);
		assertEquals("WWLWD", result.toString());
	}

	@Test
	public void printAfterSixResults(){
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.LOSS);
		assertEquals("LWWWW", result.toString());
	}
	
	// Test getNumGames() 
	
	@Test
	public void getNoResults(){
		assertEquals(0, result.getNumGames());
	}

	@Test
	public void getOneResults(){
		result.addResultToForm(WLD.WIN);
		assertEquals(1, result.getNumGames());
	}

	@Test
	public void getFiveResults(){
		result.addResultToForm(WLD.DRAW);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.LOSS);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.WIN);
		assertEquals(5, result.getNumGames());
	}

	@Test
	public void resultsAfterReset(){
		result.addResultToForm(WLD.DRAW);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.LOSS);
		result.resetForm();
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.WIN);
		assertEquals(2, result.getNumGames());
	}
	
	// test resetForm()

	@Test
	public void resetNoResults(){
		result.resetForm();
		assertEquals(0, result.getNumGames());
	}

	@Test
	public void resetOneResults(){
		result.addResultToForm(WLD.WIN);
		result.resetForm();
		assertEquals(0, result.getNumGames());
	}

	@Test
	public void resetFiveResults(){
		result.addResultToForm(WLD.DRAW);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.LOSS);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.WIN);
		result.resetForm();
		assertEquals(0, result.getNumGames());
	}

	@Test
	public void resetTwice(){
		result.addResultToForm(WLD.DRAW);
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.LOSS);
		result.resetForm();
		result.addResultToForm(WLD.WIN);
		result.addResultToForm(WLD.WIN);
		result.resetForm();
		assertEquals(0, result.getNumGames());
	}
}
